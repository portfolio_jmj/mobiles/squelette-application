class MenuMobile extends HTMLElement {
    constructor() {
        super();
        let templateMenu = document.createElement("template");
        templateMenu.innerHTML = `
        <style>
        .nav{
            position: sticky;
            bottom: 0;
            left: 0;
            right: 0;
            display: flex;
            align-items: center;
            width: 100%;
            border-radius: 24px 24px 0 0;
            backdrop-filter: blur(10px);
            -webkit-backdrop-filter: blur(10px);
        
            background-color: rgba(0, 0, 0, 0.123);
            height: 64px;
            & p{
                margin: 0;
            }
            & .nav-item{
                display: flex;
                align-items: center;
                flex-wrap: wrap;
                flex-direction: column;
                justify-content: center;
                height: 64px;
                flex: 1;
                transition: all 100ms linear;
            }
            & .nav-item:first-child{
                border-radius: 24px 0 0 0;
            }
            & .nav-item:last-child{
                border-radius: 0 24px 0 0;
            }
            & .nav-item:hover{
                background-color: rgba(0, 0, 0, 0.284);
                font-size: larger;
            }
        }
        </style>
        <nav class="nav">
        <div class="nav-item">
            <p class="nav-item-logo">🦾</p>
            <p class="nav-item-label">Menu 1</p>
        </div>
        <div class="nav-item">
            <p class="nav-item-logo">🦾</p>
            <p class="nav-item-label">Menu 1</p>
        </div>
        <div class="nav-item">
            <p class="nav-item-logo">🦾</p>
            <p class="nav-item-label">Menu 1</p>
        </div>
        <div class="nav-item">
            <p class="nav-item-logo">🦾</p>
            <p class="nav-item-label">Menu 1</p>
        </div>
        <div class="nav-item">
            <p class="nav-item-logo">🦾</p>
            <p class="nav-item-label">Menu 1</p>
        </div>
    </nav>
        `;
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.appendChild(templateMenu.content.cloneNode(true));
    }

    connectedCallback() {
        //implementation
    }

    disconnectedCallback() {
        //implementation
    }

    attributeChangedCallback(name, oldVal, newVal) {
        //implementation
    }

    adoptedCallback() {
        //implementation
    }

}

window.customElements.define('menu-mobile', MenuMobile);